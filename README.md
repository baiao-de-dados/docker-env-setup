## Projeto - Texto
Descrição do projeto.

### Team/Maintainers

<table>
  <tbody>
    <tr>
        <td align="center" valign="top">
            <img width="125" height="125" src="http://lorempixel.com/150/150/technics/Valmiro">
            <br>
            <strong>Valmiro</strong>
            <br>
            <a href="https://github.com/#">@Valmiro</a>
        </td>
        <td align="center" valign="top">
            <img width="125" height="125" src="http://lorempixel.com/150/150/technics/Alberth">
            <br>
            <strong>Alberth</strong>
            <br>
            <a href="https://github.com/#">@Alberth</a>
        </td>
        <td align="center" valign="top">
            <img width="125" height="125" src="http://lorempixel.com/150/150/technics/Samir">
            <br>
            <strong>Samir</strong>
            <br>
            <a href="https://github.com/#">@Samir</a>
        </td>
        <td align="center" valign="top">
            <img width="125" height="125" src="http://lorempixel.com/150/150/technics/Felipe">
            <br>
            <strong>Felipe</strong>
            <br>
            <a href="https://github.com/#">@Felipe</a>
        </td>
        <td align="center" valign="top">
            <img width="125" height="125" src="http://lorempixel.com/150/150/technics/Paulo">
            <br>
            <strong>Paulo</strong>
            <br>
            <a href="https://github.com/#">@Paulo</a>
        </td>
     </tr>
  </tbody>
</table>

## Sponsors

<a href="https://{company}.com" target="_blank"><img src="http://lorempixel.com/150/150/technics"></a>

<br>
Support: <a href = "mailto: support@{project}.com">support@project.com</a>.

## License

[MIT License](https://github.com/{project}/blob/master/LICENSE)